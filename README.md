# PeriPump
This is a simple speed controller for a peristaltic pump rated at 12V like
[this one](https://articulo.mercadolibre.com.ar/MLA-768684396-bomba-peristaltica-12v-arduinoetc-rosario-_JM?quantity=1).

![Picture 1](https://gitlab.com/pcremades/peripump/raw/master/images/Pic1.jpg)
