## BOM
- [Peristaltic pump](https://articulo.mercadolibre.com.ar/MLA-768684396-bomba-peristaltica-12v-arduinoetc-rosario-_JM?quantity=1).
- Arduino Nano
- A LM298 motor driver module for arduino.
- A potentiometer (any value will do).
- A 12V, 1A power supply.
- A [Nokia 5110 display](https://articulo.mercadolibre.com.ar/MLA-621168037-display-lcd-arduino-pic-arm-nokia-5110-spi-84x48-monocromo-_JM).
- An experimental [dot grid board](https://articulo.mercadolibre.com.ar/MLA-718750369-placa-experimental-perforada-5x5-pack-10-_JM) of 50mm x 50mm

## Assembly instruccions
The case is 3D printed. It was design in [FreeCAD](https://www.freecadweb.org/). You can find the STL files ready to be sliced
[here](https://gitlab.com/pcremades/peripump/raw/master/hardware).

This is the electronic schematic.
![schematic](https://gitlab.com/pcremades/peripump/raw/master/images/Schematic.png)

This is a picture of the components placed inside the case.
![Pic2](https://gitlab.com/pcremades/peripump/raw/master/images/Pic2.jpg)
You have to paste the display to the front panel of the case from the inside.

Finally, load the [firmware](https://gitlab.com/pcremades/peripump/raw/master/firmware/firmware.ino)
Use [this tool](https://gitlab.com/pcremades/peripump/raw/master/firmware/proccesImg.m) to get the binary representation of the logo and
paste it in the firmware.
